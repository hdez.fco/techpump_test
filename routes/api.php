
<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// User
Route::group(['prefix' => 'v1'], function () {
      Route::group(['prefix' => 'user'], function () {
            Route::post('register', [App\Http\Controllers\Api\V1\RegisterController::class,'createUsers']);

            Route::post('login', [App\Http\Controllers\Api\V1\LoginController::class,'loginUser'])
            ->middleware(['throttle:LimitLoginUser']);
            ;
            Route::group(['middleware' => 'auth:sanctum'], function() {
                Route::post('logout', [App\Http\Controllers\Api\V1\LogoutController::class,'logoutUser']);
            });

      });
      // Carrito de Compra
      Route::group(['prefix' => 'shopping'], function () {
        Route::group(['middleware' => 'auth:sanctum'], function() {
            Route::post('add', [App\Http\Controllers\Api\V1\ShoppingCartController::class,'addProductToCart']);
            Route::get('show', [App\Http\Controllers\Api\V1\ShoppingCartController::class,'showProductInCart']);
            Route::delete('delete', [App\Http\Controllers\Api\V1\ShoppingCartController::class,'deleteProductInCart']);
            Route::get('empty', [App\Http\Controllers\Api\V1\ShoppingCartController::class,'emptyShoppingCart']);
            Route::post('update', [App\Http\Controllers\Api\V1\ShoppingCartController::class,'updateShoppingCart']);
            Route::post('buy', [App\Http\Controllers\Api\V1\BuyController::class,'processBuy']);
        });
  });
});
