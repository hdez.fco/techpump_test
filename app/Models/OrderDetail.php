<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\config\Main\GlobalConstantsDatabase;

class OrderDetail extends Model
{


    protected $table =  'orders_details';

    protected $fillable = [

        'order_id',
        'product_id',
        'value',
        'amount',
        'sub_total',
    ];


}
