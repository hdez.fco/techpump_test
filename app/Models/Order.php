<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\config\Main\GlobalConstantsDatabase;

class Order extends Model
{


    protected $table =  'orders';

    protected $fillable = [

        'user_id',
        'confirmed',
        'reference',
        'status',
        'mount',
    ];


}
