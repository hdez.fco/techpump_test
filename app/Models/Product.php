<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\config\Main\GlobalConstantsDatabase;

class Product extends Model
{


    protected $table =  'products';

    protected $fillable = [
        'name',
        'value',
        'image',
        'available',
        'in_process',
        'category_id',
    ];


}
