<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\config\Main\GlobalConstantsDatabase;

class Category extends Model
{


    protected $table =  'categorys';

    protected $fillable = [
        'id',
        'name',
    ];


}
