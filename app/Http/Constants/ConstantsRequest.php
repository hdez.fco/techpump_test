<?php
namespace  App\Http\Constants;

class ConstantsRequest
{

    const RESP_OK  = true;
    const RESP_BAD = false;

    const HTTP = [
                    'Ok'                            => 200,
                    'BadRequest'                    => 400,
                    'SaleActive'                    => 403,
                    'Unauthorized'                  => 401,
                    'NotFound'                      => 404,
                    'UnprocessableEntity'           => 422,
                    'Conflict'                      => 500,
                    'TooManyRequest'                => 419
                 ];

    const MENS = [
                    200 => [self::RESP_OK,'Exito',['Operación realizada de forma satisfactoria']],

                    400 => [self::RESP_BAD,'Datos Suministrados no son válidos',['Datos de entrada errados ó Registro no existe']] ,
                    401 => [self::RESP_BAD,'No Autorizado',['Error en credenciales','Solicitud no procesada','No está autorizado sobre este recurso']],
                    403 => [self::RESP_BAD,'Solicitud No Procesada',['Usuario con un proceso de venta activo en proceso']],
                    404 => [self::RESP_BAD,'Datos no registrados',['Recurso no disponible', 'Verifique información']],
                    422 => [self::RESP_BAD,'Imposible procesar',['Inconsistencia en datos','informaciòn solicitada no está disponible']],
                    500 => [self::RESP_BAD,'Error interno',['Servicio invocado retona error en el servidor']],
                    419 => [self::RESP_BAD,'Error interno',['Muchos intentos errados']],
                 ];


     const SALE = [
                    'PHASE_A'  => 'EN PROCESO DE PAGO',
                    'PHASE_B'  => 'PROCESO DE PAGO CANCELADO',
                    'PHASE_C'  => 'PROCESO DE PAGO VALIDADO',
                    'PHASE_D'  => 'PROCESO DE PAGO EXITOSO',
                 ];
}
