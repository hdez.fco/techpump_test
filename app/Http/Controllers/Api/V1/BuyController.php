<?php

namespace App\Http\Controllers\Api\V1;


use Carbon\Carbon;
use App\Models\Order;
use App\Models\Product;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Traits\ResponseTrait;
use App\Services\DatabaseServices;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Http\Constants\ConstantsRequest;
use App\Events\DestroySalesReservationEvent;
use App\Http\Controllers\Api\V1\ShoppingCartController;

class BuyController extends Controller
{
    //
    /**
    * @OA\Post(
    * path="/api/v1/shopping/buy",
    * summary="Buy the products",
    * description="Buy products present shopping cart. Please see Shema data Forma",
    * operationId="buyCart",
    * tags={"shopping cart"},
    * security={{"bearer_token":{}}},
    *  @OA\Response(
    *      response=200,
    *      description="Successful operation",
    *       @OA\JsonContent(
    *          @OA\Property(property="mes", type="string", example="Exito"),
    *      )
    * ),
    * @OA\Response(
    *      response=400,
    *      description="failed operation",
    *      @OA\JsonContent(
    *          @OA\Property(property="mes", type="string", example="Datos Suministrados no son válido"),
    *      )
    *   ),
    * )
    */
    public function processBuy(Request $request)
    {
        try {
            DB::beginTransaction();
            $sale = (new ShoppingCartController)->getDetailSale($request->user()->id);
            if (!Cache::has($sale)) {
                $result = ['El carrito está vacio'];
                $code    = ConstantsRequest::HTTP['BadRequest'];
            }else{
                $data = [
                    'user_id'   => $request->user()->id,
                    'confirmed' => false,
                    'reference' => self::generateReference(),
                    'status'    => ConstantsRequest::SALE['PHASE_A'],
                    'mount'     =>(new ShoppingCartController)->getTotalMount(Cache::get($sale)),

                ];
                $result = (new DatabaseServices)->CreateRecord(New Order, $data);

                $dataDetailMaster = [];
                $saleDetail = Cache::get($sale);
                foreach ($saleDetail as $key => $ItemInput) {
                    $product = Product::find($ItemInput['id']);
                        if ($product){
                            $dataDetail = [
                                'order_id'   =>$result->id,
                                'product_id' =>$ItemInput['id'],
                                'value'      => $product->value,
                                'amount'     => $ItemInput['cant'],
                                'sub_total'  => ($product->value*$ItemInput['cant']),
                                'created_at'   => Carbon::now(),
                                'updated_at'   => Carbon::now()

                            ];
                            $dataDetailMaster[] =  $dataDetail;
                        };
                }
                $result = (new DatabaseServices)->InsertMultipleRecord((New OrderDetail)->getTable(), $dataDetailMaster);
                $code    = ConstantsRequest::HTTP['Ok'];
                $saleEfective = true;
                event(new DestroySalesReservationEvent((new ShoppingCartController)->getDetailSale($request->user()->id),$saleEfective));
            }
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            $code    = ConstantsRequest::HTTP['UnprocessableEntity'];
            $result = null;
        }
        return ResponseTrait::responseApi($code,$result);
    }
    //
    static function generateReference()
    {
        return time().'-'.random_int(100,999);
    }


}
