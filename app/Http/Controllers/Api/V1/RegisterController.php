<?php

namespace App\Http\Controllers\Api\V1;

use Validator;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Http\Request;
use App\Services\DatabaseServices;
use App\Http\Traits\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Http\Constants\ConstantsRequest;

/**
    * @OA\Post(
    * path="/api/v1/user/register",
    * summary="Register User",
    * description="Register user (Type Admin/Standar)",
    * operationId="userRegister",
    * tags={"user"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Input data",
    *    @OA\JsonContent(
    *       required={"name","email,password,role_id"},
    *       @OA\Property(property="name", type="string", format="string", example="admin"),
    *       @OA\Property(property="email", type="string", format="email", example="admin@local.com"),
    *       @OA\Property(property="password", type="string", format="password", example="123456"),
    *       @OA\Property(property="role_id", type="numeric", format="numeric", example="1"),
    *    ),
    * ),
    * @OA\Response(
    *      response=200,
    *      description="Successful operation",
    *       @OA\JsonContent(
    *          @OA\Property(property="mes", type="string", example="Exito"),
    *      )
    * ),
    * @OA\Response(
    *      response=400,
    *      description="Datos Suministrados no son válidos",
    *      @OA\JsonContent(
    *          @OA\Property(property="mes", type="string", example="Datos Suministrados no son válidos"),
    *      )
    *   ),
    * )
    */
class RegisterController extends Controller
{
    public function createUsers (Request $request)
    {
        try {
            $code    = ConstantsRequest::HTTP['Unauthorized'];

            $validator = Validator::make($request->all(), (new CreateUserRequest)->rules());
            if ($validator->fails()) {
                $code    = ConstantsRequest::HTTP['BadRequest'];
                return ResponseTrait::responseApi($code,$validator->errors());
            }

            $data  = [
                    'name'        => $request->name,
                    'email'       => $request->email,
                    'password'    => bcrypt($request->password),
                    'role_id'     => $request->role_id,
                    'created_at'  => Carbon::now(),
                    'update_at'   => Carbon::now()
                ];

            $result = (new DatabaseServices)->CreateRecord(New User, $data);
            $code    = is_null($result)?ConstantsRequest::HTTP['UnprocessableEntity']:ConstantsRequest::HTTP['Ok'];

        } catch (\Throwable $th) {
            $code    = ConstantsRequest::HTTP['UnprocessableEntity'];
            $result = null;
        }

        return ResponseTrait::responseApi($code,$result);
    }

}
