<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Constants\ConstantsRequest;
use App\Events\DestroySalesReservationEvent;
use App\Listeners\DestroyReservationListener;
use App\Http\Controllers\Api\V1\ShoppingCartController;

/**
 * @OA\Post(
    * path="/api/v1/user/logout",
    * summary="Logout",
    * description="Logout user",
    * operationId="userLogout",
    * tags={"user"},
    * security={{"bearer_token":{}}},
    *
    *  @OA\Response(
    *      response=200,
    *      description="Successful operation",
    *       @OA\JsonContent(
    *          @OA\Property(property="mes", type="string", example="Exito"),
    *      )
    * ),
    * @OA\Response(
    *      response=401,
    *      description="failed operation",
    *      @OA\JsonContent(
    *          @OA\Property(property="mes", type="string", example="No autorizado"),
    *      )
    *   ),
    * )
 */
class LogoutController extends Controller
{
    public function logoutUser(Request $request)
    {
        try {
            if(Auth::check()) {
                $close = $request->user()->tokens()->delete();
                $saleEfective = false;
                event(new DestroySalesReservationEvent((new ShoppingCartController)->getDetailSale($request->user()->id), $saleEfective));
                $code = ($close)? ConstantsRequest::HTTP['Ok']:ConstantsRequest::HTTP['Conflict'];
                $men  = ($close)?['Se Ha Cerrado La Session']:['Error al cerrar la session'];
            }
        } catch (\RuntimeException $e) {
              $code   = ConstantsRequest::HTTP['Conflict'];
              $men    = ['Error al cerrar la session'];
        }
        return ResponseTrait::responseApi($code,[],$men);
     }
}
