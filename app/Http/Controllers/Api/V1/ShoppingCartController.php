<?php

namespace App\Http\Controllers\Api\V1;

use Validator;
use Carbon\Carbon;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Services\DatabaseServices;
use App\Http\Traits\ResponseTrait;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Http\Constants\ConstantsRequest;
use App\Events\DestroySalesReservationEvent;
use App\Http\Requests\AddProductTheCarRequest;

class ShoppingCartController extends Controller
{
    /**
    * @OA\Post(
    * path="/api/v1/shopping/add",
    * summary="add product to shopping cart",
    * description="add product. Please input array of object Please see Shema data Forma.  Ej data input:  : [{id:1,cant:2},{id:2,cant:3}]",
    * operationId="addCart",
    * tags={"shopping cart"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *             required=true,
    *             @OA\JsonContent(
    *                required={"id","cant"},
    *               @OA\Property(property="id", type="integer", format="numeric", example="1"),
    *               @OA\Property(property="cant", type="integer", format="numeric", example="2"),
    *             )
    *         ),
    *  @OA\Response(
    *      response=200,
    *      description="Successful operation",
    *       @OA\JsonContent(
    *          @OA\Property(property="mes", type="string", example="Exito"),
    *      )
    * ),
    * @OA\Response(
    *      response=422,
    *      description="failed operation",
    *      @OA\JsonContent(
    *          @OA\Property(property="mes", type="string", example="Imposible procesar"),
    *      )
    *   ),
    * )
    */
    public function addProductToCart(Request $request)
    {
        try {
            DB::beginTransaction();
            $code    = ConstantsRequest::HTTP['BadRequest'];
            $saleDetail = $request->all();

            foreach ($saleDetail as $key => $ItemInput) {
                $validator = Validator::make($ItemInput, (new AddProductTheCarRequest)->rules());
                if ($validator->fails()) {
                    $validator->getMessageBag()->add('id', 'id Producto posición '.$key);
                    $code    = ConstantsRequest::HTTP['BadRequest'];
                    return ResponseTrait::responseApi($code,$validator->errors());
                }
                $product = Product::find($ItemInput['id']);
                if (($product->available-$ItemInput['cant']) < 0){
                    $code = ConstantsRequest::HTTP['BadRequest'];
                    $men  = self::getDataProduct($product,$key);
                    DB::rollback();
                    return ResponseTrait::responseApi($code,$men);
                }else{
                    $product->increment('in_process', $ItemInput['cant']);
                    $product->decrement('available',$ItemInput['cant']);
                };

            }

            $sale = $this->getDetailSale($request->user()->id);

            if (!Cache::has($sale)) {
                Cache::rememberForever($sale, function() use ($saleDetail){
                return $saleDetail;
                });
            }else{
                $saleDetail = array_merge(Cache::get($sale),$saleDetail);
                Cache::forget($sale);
                Cache::rememberForever($sale, function() use ($saleDetail){
                    return $saleDetail;
                });
            }
            $result = self::getDetailCart($saleDetail);
            $code    = ConstantsRequest::HTTP['Ok'];
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            $code    = ConstantsRequest::HTTP['UnprocessableEntity'];
            $result = null;
        }
        return ResponseTrait::responseApi($code,$result);
    }
    //
    /**
    * @OA\Get(
    * path="/api/v1/shopping/show",
    * summary="show shopping cart",
    * description="Show the products in shopping cart",
    * operationId="showCart",
    * tags={"shopping cart"},
    * security={{"bearer_token":{}}},
    *  @OA\Response(
    *      response=200,
    *      description="Successful operation",
    *       @OA\JsonContent(
    *          @OA\Property(property="mes", type="string", example="Exito"),
    *      )
    * ),
    * @OA\Response(
    *      response=422,
    *      description="failed operation",
    *      @OA\JsonContent(
    *          @OA\Property(property="mes", type="string", example="Imposible procesar"),
    *      )
    *   ),
    * )
    */
    public function showProductInCart(Request $request)
    {
        try {
            $sale = $this->getDetailSale($request->user()->id);

            if (!Cache::has($sale)) {
                $saleDetail = [];
            }else{
                $saleDetail = Cache::get($sale);
            }
            $saleInProcess = [];
            foreach ($saleDetail as $key => $products) {
                $saleInProcess[$products['id']] = ['id'=>$products['id'],'cant'=>0];
            }

            foreach ($saleDetail as $key => $products) {
                $saleInProcess[$products['id']]['cant'] = $saleInProcess[$products['id']]['cant']+$products['cant'];
            }

            $result = self::getDetailCart(array_values($saleInProcess));
            $code    = ConstantsRequest::HTTP['Ok'];
        } catch (\Throwable $th) {
            $code    = ConstantsRequest::HTTP['UnprocessableEntity'];
            $result = null;
        }
        return ResponseTrait::responseApi($code,$result);
    }
    //
    /**
    * @OA\Delete(
    * path="/api/v1/shopping/delete",
    * summary="Delete product in shopping cart",
    * description="Delete the products in shopping cart. Please see Shema data Forma.  Ej data input:  : [{id:1,cant:2},{id:2,cant:3}]",
    * operationId="deleteCart",
    * tags={"shopping cart"},
    * security={{"bearer_token":{}}},
    *  @OA\RequestBody(
    *             required=true,
    *             @OA\JsonContent(
    *                required={"id","cant"},
    *               @OA\Property(property="id", type="integer", format="numeric", example="1"),
    *               @OA\Property(property="cant", type="integer", format="numeric", example="2"),
    *             )
    *         ),
    *  @OA\Response(
    *      response=200,
    *      description="Successful operation",
    *       @OA\JsonContent(
    *          @OA\Property(property="mes", type="string", example="Exito"),
    *      )
    * ),
    * @OA\Response(
    *      response=422,
    *      description="failed operation",
    *      @OA\JsonContent(
    *          @OA\Property(property="mes", type="string", example="Imposible procesar"),
    *      )
    *   ),
    * )
    */
    public function deleteProductInCart(Request $request)
    {
        try {
            DB::beginTransaction();
            $code    = ConstantsRequest::HTTP['BadRequest'];
            $saleDetail = $request->all();
            $sale = $this->getDetailSale($request->user()->id);
            $delete = false;
            $current = Cache::get($sale);
            foreach ($saleDetail as $key => $ItemInput) {
                $rules =  (new AddProductTheCarRequest)->rules();
                unset($rules['cant']);

                $validator = Validator::make($ItemInput,$rules);
                if ($validator->fails()) {
                    $validator->getMessageBag()->add('id', 'id Producto posición '.$key);
                    $code    = ConstantsRequest::HTTP['BadRequest'];
                    return ResponseTrait::responseApi($code,$validator->errors());
                }

                foreach ($current as $key2 => $currentProduct) {
                    if ($currentProduct['id'] == $ItemInput['id']){
                        $product = Product::find($ItemInput['id']);
                        if ($product){
                            $delete = true;
                            $product->decrement('in_process', $currentProduct['cant']);
                            $product->increment('available',$currentProduct['cant']);
                        };
                        unset($current[$key2]);
                    }
                }
            }

            if (!$delete) {
                $code = ConstantsRequest::HTTP['BadRequest'];
                $men  = ['El producto no está en el carrito'];
                return ResponseTrait::responseApi($code,$men);
            }
            $saleDetail = array_values($current);
            Cache::forget($sale);
            Cache::rememberForever($sale, function() use ($saleDetail){
                return $saleDetail;
            });

            $result = self::getDetailCart($saleDetail);
            $code    = ConstantsRequest::HTTP['Ok'];
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            $code    = ConstantsRequest::HTTP['UnprocessableEntity'];
            $result = null;
        }
        return ResponseTrait::responseApi($code,$result);
    }
    //
    //
    /**
    * @OA\Get(
    * path="/api/v1/shopping/empty",
    * summary="Empty shopping cart",
    * description="Empty shopping cart",
    * operationId="emptyCart",
    * tags={"shopping cart"},
    * security={{"bearer_token":{}}},
    *  @OA\Response(
    *      response=200,
    *      description="Successful operation",
    *       @OA\JsonContent(
    *          @OA\Property(property="mes", type="string", example="Exito"),
    *      )
    * ),
    * @OA\Response(
    *      response=422,
    *      description="failed operation",
    *      @OA\JsonContent(
    *          @OA\Property(property="mes", type="string", example="Imposible procesar"),
    *      )
    *   ),
    * )
    */
    public function emptyShoppingCart(Request $request)
    {
        $saleEfective = false;
        event(new DestroySalesReservationEvent((new ShoppingCartController)->getDetailSale($request->user()->id), $saleEfective));
        $code    = ConstantsRequest::HTTP['Ok'];
        $result  = [];

        return ResponseTrait::responseApi($code,$result);
    }
    //
    /**
    * @OA\Post(
    * path="/api/v1/shopping/update",
    * summary="Update product in shopping cart",
    * description="Update the products in shopping cart. Please see Shema data Forma.  Ej data input:  : [{id:1,cant:2},{id:2,cant:3}]",
    * operationId="updateCart",
    * tags={"shopping cart"},
    * security={{"bearer_token":{}}},
    *  @OA\RequestBody(
    *             required=true,
    *             @OA\JsonContent(
    *                required={"id","cant"},
    *               @OA\Property(property="id", type="integer", format="numeric", example="1"),
    *               @OA\Property(property="cant", type="integer", format="numeric", example="2"),
    *             )
    *         ),
    *  @OA\Response(
    *      response=200,
    *      description="Successful operation",
    *       @OA\JsonContent(
    *          @OA\Property(property="mes", type="string", example="Exito"),
    *      )
    * ),
    * @OA\Response(
    *      response=422,
    *      description="failed operation",
    *      @OA\JsonContent(
    *          @OA\Property(property="mes", type="string", example="Imposible procesar"),
    *      )
    *   ),
    * )
    */
    public function updateShoppingCart(Request $request)
    {
        try {
            DB::beginTransaction();
            $code    = ConstantsRequest::HTTP['BadRequest'];
            $saleDetail = $request->all();
            $sale = $this->getDetailSale($request->user()->id);
            $update = false;
            $current = (Cache::has($sale))?Cache::get($sale):[];
            foreach ($saleDetail as $key => $ItemInput) {
                $rules =  (new AddProductTheCarRequest)->rules();

                $validator = Validator::make($ItemInput,$rules);
                if ($validator->fails()) {
                    $validator->getMessageBag()->add('id', 'id Producto posición '.$key);
                    $code    = ConstantsRequest::HTTP['BadRequest'];
                    return ResponseTrait::responseApi($code,$validator->errors());
                }
                $readyIsPresent = false;
                foreach ($current as $key2 => $currentProduct ) {
                    if ($currentProduct['id'] == $ItemInput['id'] && !$readyIsPresent){
                        $product = Product::find($ItemInput['id']);
                        if ($product){
                            $update = true;
                            $product->decrement('in_process', $currentProduct['cant']);
                            $product->increment('available',$currentProduct['cant']);
                            if (($product->available - $ItemInput['cant'])< 0){
                                $code = ConstantsRequest::HTTP['BadRequest'];
                                $men  = self::getDataProduct($product,$key2);
                                return ResponseTrait::responseApi($code,$men);
                            }
                            $product->increment('in_process', $ItemInput['cant']);
                            $product->decrement('available', $ItemInput['cant']);
                        };
                        $readyIsPresent  = true;
                        $current[$key2] = ['id'=>$ItemInput['id'],'cant'=>$ItemInput['cant']];
                    }
                }
                if (!$readyIsPresent){
                    $update = true;
                    $product = Product::find($ItemInput['id']);
                    $current = array_merge($current,[$ItemInput]);
                    $product->increment('in_process', $ItemInput['cant']);
                    $product->decrement('available', $ItemInput['cant']);
                }
            }

            if (!$update) {
                $code = ConstantsRequest::HTTP['BadRequest'];
                $men  = ['El producto no está en el carrito'];
                return ResponseTrait::responseApi($code,$men);
            }
            $saleDetail = array_values($current);
            Cache::forget($sale);
            Cache::rememberForever($sale, function() use ($saleDetail){
                return $saleDetail;
            });

            $result = self::getDetailCart($saleDetail);
            $code    = ConstantsRequest::HTTP['Ok'];
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            $code    = ConstantsRequest::HTTP['UnprocessableEntity'];
            $result = null;
        }
        return ResponseTrait::responseApi($code,$result);

    }
    //
    protected function getDetailCart($cart){
        $result = ['items'    =>count($cart),
                    'products'=>array_sum(array_column($cart, 'cant')),
                    'total'   =>$this->getTotalMount($cart),
                    'detail'  =>$cart
                   ];

        return $result;
    }
    //
    static function getDataProduct($product,$key)
    {
        return [
            'id Producto: '.$product->id,
            'Cantidad solicitada no disponible',
            'Nombre: '.$product->name,
            'Disponible: '.$product->available,
            'Reservados: '.$product->in_process,
           ];
    }
    //
    public function getTotalMount($cart)
    {
        $wtotal = 0;
        foreach ($cart as $key => $ItemInput) {
            $product = Product::find($ItemInput['id']);
            if ($product){
                $wtotal = $wtotal + ($product->value)*$ItemInput['cant'];
            }
        }
        return $wtotal;
    }
    //
    public function getDetailSale($userId){
        $sale = 'SaleInProcessUser_'.$userId;
        return $sale;
    }
}
