<?php

namespace App\Http\Controllers\Api\V1;

use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Services\DatabaseServices;
use App\Http\Traits\ResponseTrait;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Constants\ConstantsRequest;

class LoginController extends Controller
{
    /**
    * @OA\Post(
    * path="/api/v1/user/login",
    * summary="Sign in",
    * description="Login by email, password",
    * operationId="userLogin",
    * tags={"user"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Pass user credentials",
    *    @OA\JsonContent(
    *       required={"email","password"},
    *       @OA\Property(property="email", type="string", format="email", example="admin@admin.com"),
    *       @OA\Property(property="password", type="string", format="password", example="123456"),
    *    ),
    * ),
    * @OA\Response(
    *      response=200,
    *      description="Successful operation",
    *       @OA\JsonContent(
    *          @OA\Property(property="mes", type="string", example="Exito"),
    *      )
    * ),
    * @OA\Response(
    *      response=401,
    *      description="failed operation",
    *      @OA\JsonContent(
    *          @OA\Property(property="mes", type="string", example="Not autorizado"),
    *      )
    *   ),
    * )
    */
    public function loginUser(Request $request)
    {
        try {
            DB::beginTransaction();
            $result = [];
            $code    = ConstantsRequest::HTTP['BadRequest'];

            $validator = Validator::make($request->all(), ['email'=>'required','password'=>'required']);
            if ($validator->fails()) {
                return ResponseTrait::responseApi($code,$validator->errors());
            }
            $aMessage = [];
            $credentials = request(['email', 'password']);
            if (!Auth::attempt($credentials)) {
                $code   =  ConstantsRequest::HTTP['Unauthorized'];
                $result = [];
            }else{
                $user = auth()->user();

                $role  = ($user->role_id==1)?'Administrador':'Estandar';

                $token = $user->createToken('TEST',[$role])->plainTextToken;
                $code    = ConstantsRequest::HTTP['Ok'];
                $result = [  'user'=> [ 'email'     =>$user->email,
                                        'name'      =>$user->name,
                                    ],
                                'token'=>['id'         => $token,
                                        'type'       => 'Bearer',
                                        ],
                        ];
            }
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            $code   = ConstantsRequest::HTTP['Conflict'];
            $result = [];
            $aMessage = [$th->getMessage()];
        }

        return ResponseTrait::responseApi($code,$result,$aMessage);

    }
}
