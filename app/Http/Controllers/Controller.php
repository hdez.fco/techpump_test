<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
    * @OA\Info(
    *    title="Documentación TEST Api Techpump - Carrito de Compra",
    *    version="1.0.0",
    * )
    *  @OA\Server(url="http://localhost:8000")
    *
    * @OA\SecurityScheme(
    *     type="apiKey",
    *     description="Login with email and password to get the authentication token",
    *     name="Token based Sactum",
    *     in="header",
    *     scheme="sanctum_token",
    *     securityScheme="sanctum_token",
    * ),
    * @OA\Schema(
    *      schema="InputData",
    *      type="array",
    *      @OA\Items(
    *          required={"id", "cant"},
    *          @OA\Property(property="id", type="integer"),
    *          @OA\Property(property="cant", type="integer")
    *      )
    * )
     */
class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;
}
