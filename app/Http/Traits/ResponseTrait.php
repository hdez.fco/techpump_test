<?php

namespace App\Http\Traits;

use App\Http\Constants\ConstantsRequest;

trait ResponseTrait {
    static function responseApi($code,$result=[],$anexo=[]) {

        $message = array_merge(ConstantsRequest::MENS[$code][2],$anexo);

          if (in_array($code,ConstantsRequest::HTTP)){
              $message = (count($anexo))?array_merge($anexo,ConstantsRequest::MENS[$code][2]):ConstantsRequest::MENS[$code][2];
              return response()->json(['res'=> ConstantsRequest::MENS[$code][0],'cod'=> $code,'mes'=> ConstantsRequest::MENS[$code][1] ,'det' => $message,'data'=>$result], 200);
          }else{
              $code   = ConstantsRequest::HTTP['ImATeapot'];
              return response()->json(['res'=> ConstantsRequest::MENS[$code][0],'cod'=> $code,'mes'=> ConstantsRequest::MENS[$code][1] ,'det' => ConstantsRequest::MENS[$code][2],'data'=>$result], 200);
          }
    }
}
