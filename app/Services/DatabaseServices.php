<?php

namespace App\Services;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class DatabaseServices extends Controller
{
    public function CreateRecord($model,$data)
    {
        try {
            DB::beginTransaction();
            $createRecord = $model::create($data);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            $createRecord = null;
        }
        return $createRecord;
    }

    public function InsertMultipleRecord($model,$data)
    {
        try {
            DB::beginTransaction();
            $createRecord = DB::table($model)->insert($data);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            $createRecord = null;
        }
        return ($createRecord)?  ['Iniciado el Proceso de Compra']:['Falló el proceso de compra'];
    }
}
