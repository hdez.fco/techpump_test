<?php

namespace App\Listeners;

use App\Models\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class DestroyReservationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        try {
            DB::beginTransaction();
            $sale = $event->reserve;
            if (Cache::has($sale)) {
                $saleDetailDestroy = Cache::get($sale);

                foreach ($saleDetailDestroy as $key => $ItemInput) {
                    $product = Product::find($ItemInput['id']);
                    if ($product && !$event->saleEfective){
                        $product->decrement('in_process', $ItemInput['cant']);
                        $product->increment('available',$ItemInput['cant']);
                    }
                };
                Cache::forget($sale);
            }
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
        }
    }
}
