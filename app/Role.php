<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\config\Main\GlobalConstantsDatabase;

class Role extends Model
{


    protected $table =  'roles';

    protected $fillable = [
        'id',
        'name',
    ];


}
