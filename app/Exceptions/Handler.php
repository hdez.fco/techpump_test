<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Http\Traits\ResponseTrait;
use App\Http\Constants\ConstantsRequest;
use Illuminate\Auth\AuthenticationException;

use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });

    }

    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    public function render($request, Throwable $exception)
    {

        if ($exception instanceof \Symfony\Component\Routing\Exception\RouteNotFoundException) {
            $code = ConstantsRequest::HTTP['Unauthorized'];
            $anexo=['Debe iniciar sessión'];
            return ResponseTrait::responseApi($code,[],$anexo);
        }

        if ($exception->getMessage() =="Too Many Attempts."){

            $code = ConstantsRequest::HTTP['TooManyRequest'];
            $anexo = ["Intente nuevamente en: ". $exception->getHeaders()['Retry-After']." segundo(s)"];
            return ResponseTrait::responseApi($code,[],$anexo);

        }
        if ($exception instanceof  AuthenticationException ) {
            $code = ConstantsRequest::HTTP['Unauthorized'];
            $anexo=['Debe iniciar sessión'];
            return ResponseTrait::responseApi($code,$anexo);
        }

        return parent::render($request, $exception);
    }
}
