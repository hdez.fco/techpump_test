PROYECTO: TEST TECHPUMP

HERRAMIENTAS UTILIZADAS

a) PHP 8.2.7 (cli) (built: Jun  8 2023 15:27:12) (NTS)
Copyright (c) The PHP Group
Zend Engine v4.2.7, Copyright (c) Zend Technologies
    with Zend OPcache v8.2.7, Copyright (c), by Zend Technologies

b) mysql  Ver 8.0.33

c) Laravel Framework 10.13.5

d) SO Linux - Servicio WEB Apache2

e) Insomnia 2022.6.0

f) VSC

PROCESO DE INSTALACION

1. composer install
2. php artisan migrate
3. php artisan cache:clear
4. php artisan view:clear
5. php artisan route:clear
6. php artisan config:clear
7. php artisan migrate:refresh
8. php artisan db:seed
9. php artisan l5-swagger:generate  (DOCUMENTACION API)
10. php artisan serve

DOCUMENTACION

1. http://{{SERVIDOR}}:[PUERTO_DE_ESCUCHA_LARAVEL]/api/documentation#/user/userLogout

Ejemplo:
http://localhost:8000/api/documentation#

EJECUCION DE END POINT EN LA DOCUMENTACION

1. Ejecute el servicio Login
2. Copie el valor del token
3. Pulse el boton AUTORIZE
4. Asigne el valor copiado a los campor sanctum_token (apiKey) y bearer_token (apiKey)
5. Pruebe los servicios

ARCHIVO DE LA COLEECION DE ENDPOINT

1. Disponibl en:
[PROYECTO]/app/Documentacion/insomnia_cart.json

Notas de interés

1. Se permite sólo 5 intentos de login errado por minuto
2. Se realizaron 2 versiones del proyecto:
	a) Autenticación con Laravel Passport (Mucho más eficiente - Solo que las versiones generaron conflicto con phpUnit en mi máquina local)
	b) Autenticación con Laravel Sactum
3. A efectos de generar la documentación se requiere permiso de escritura en storage/
