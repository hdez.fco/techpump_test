<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Tests\TestCase;
use App\Models\User;
use App\Models\Product;
use Illuminate\Support\Facades\Cache;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmptyTheCartTest extends TestCase
{
    public function test_can_empty_product_to_cart()
    {
        $user = User::find(1);
        $role  = ($user->role_id==1)?'Administrador':'Estandar';

        $token = $user->createToken('TEST',[$role])->plainTextToken;

        $headers = ['Authorization' => "Bearer $token"];

        $this->actingAs($user);
        Cache::forget('SaleInProcessUser_' . $user->id);

        $route = '/api/v1/shopping/add';

        $product = Product::find(1);

        $payload = [
            [
                'id' => $product->id,
                'cant' => 2,
            ],
        ];

        $response = $this->actingAs($user)
                         ->postJson($route, $payload);

        $response->assertStatus(200);

        $product->refresh();
        $this->assertEquals(8, $product->available);
        $this->assertEquals(2, $product->in_process);

        $this->assertTrue(Cache::has('SaleInProcessUser_' . $user->id));
        $this->assertEquals($payload, Cache::get('SaleInProcessUser_' . $user->id));

        $route = '/api/v1/shopping/empty';

        $product = Product::find(1);

        $response = $this->json('GET', $route, $headers);

        $product->refresh();
        $this->assertEquals(10, $product->available);
        $this->assertEquals(0, $product->in_process);
        Cache::forget('SaleInProcessUser_' . $user->id);
        Cache::rememberForever('SaleInProcessUser_' . $user->id, function(){
            return [];
        });

        $this->assertEquals([], Cache::get('SaleInProcessUser_' . $user->id));
        $response->assertStatus(200)
        ->assertSee(Cache::get('SaleInProcessUser_' . $user->id));
    }

    protected function setUp(): void
    {
        parent::setUp();
        user::truncate();
        User::create([

            'id'        => 1,
            'name'      => "Admin",
            'email'     => "admin@admin.com",
            'password'  => bcrypt('123456'),
            'role_id'   => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now()
        ]);
        Product::truncate();
        Product::create([
            'id' => 1,
            'name' => 'Bicicleta',
            'value' => 1000,
            'image' => null,
            'available' => 10,
            'in_process' =>0,
            'category_id'=> 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now()
            ]);

    }
}
