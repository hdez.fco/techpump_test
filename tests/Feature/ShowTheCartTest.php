<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Facades\Cache;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShowTheCartTest extends TestCase
{
    public function test_show_the_cart()
    {
        $route = '/api/v1/shopping/show';

        $user = User::find(1);

        $role  = ($user->role_id==1)?'Administrador':'Estandar';

        $token = $user->createToken('TEST',[$role])->plainTextToken;

        $headers = ['Authorization' => "Bearer $token"];

        $this->actingAs($user);

        $response = $this->json('GET', $route, $headers);

        $response->assertStatus(200)
        ->assertSee(Cache::get('SaleInProcessUser_' . $user->id));

    }
    protected function setUp(): void
    {
        parent::setUp();
        user::truncate();
        User::create([

            'id'        => 1,
            'name'      => "Admin",
            'email'     => "admin@admin.com",
            'password'  => bcrypt('123456'),
            'role_id'   => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now()
        ]);

    }
}
