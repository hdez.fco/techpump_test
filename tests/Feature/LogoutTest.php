<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LogoutTest extends TestCase
{
    use RefreshDatabase;

    public function setUp() :void
    {
        parent::setUp();
        user::truncate();
        User::create([

            'id'        => 1,
            'name'      => "Admin",
            'email'     => "admin@admin.com",
            'password'  => bcrypt('123456'),
            'role_id'   => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now()
        ]);

    }

    public function test_logout_user()
    {
        $route = '/api/v1/user/logout';

        $user = User::find(1);

        $role  = ($user->role_id==1)?'Administrador':'Estandar';

        $token = $user->createToken('TEST',[$role])->plainTextToken;

        $headers = ['Authorization' => "Bearer $token"];

        $this->actingAs($user);

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->json('POST', $route, $headers);

        $response->assertStatus(200);


    }
}
