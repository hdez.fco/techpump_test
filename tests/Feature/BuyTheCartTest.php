<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Tests\TestCase;
use App\Models\User;
use App\Models\Order;
use App\Models\Product;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\Cache;
use App\Http\Constants\ConstantsRequest;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BuyTheCartTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        user::truncate();
        User::create([

            'id'        => 1,
            'name'      => "Admin",
            'email'     => "admin@admin.com",
            'password'  => bcrypt('123456'),
            'role_id'   => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now()
        ]);
        Product::truncate();
        Product::create([
            'id' => 1,
            'name' => 'Bicicleta',
            'value' => 1000,
            'image' => null,
            'available' => 10,
            'in_process' =>0,
            'category_id'=> 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now()
            ]);
    }
    public function test_buy_cart_empty()
    {

        $route = '/api/v1/shopping/buy';

        $user = User::find(1);
        $role  = ($user->role_id==1)?'Administrador':'Estandar';

        $token = $user->createToken('TEST',[$role])->plainTextToken;

        $headers = ['Authorization' => "Bearer $token"];

        $this->actingAs($user);

        Cache::forget('SaleInProcessUser_' . $user->id);

        $response = $this->json('POST', $route, $headers);

        $response->assertStatus(200);

        $response->assertJson([
            "res"=> false,
            "cod"=> 400,
            "mes"=> "Datos Suministrados no son válidos",
            "det"=> [
                "Datos de entrada errados ó Registro no existe"
            ],
            "data"=> [
                "El carrito está vacio"
            ]
        ]);

    }

    public function test_buy_cart_not_empty()
    {

        $route = '/api/v1/shopping/buy';

        $user = User::find(1);
        $role  = ($user->role_id==1)?'Administrador':'Estandar';

        $token = $user->createToken('TEST',[$role])->plainTextToken;

        $headers = ['Authorization' => "Bearer $token"];

        Cache::rememberForever('SaleInProcessUser_' . $user->id, function(){
            return [0=>['id'=>1,'cant'=>1]];
        });
        $saleDetail = Cache::get('SaleInProcessUser_' . $user->id);

        $data = [
            'user_id'   => $user->id,
            'confirmed' => false,
            'reference' => '1111',
            'status'    => ConstantsRequest::SALE['PHASE_A'],
            'mount'     =>2000,

        ];
        $result = Order::create($data);
        $dataDetailMaster=[];
        foreach ($saleDetail as $key => $ItemInput) {
            $product = Product::find($ItemInput['id']);
            $order = Order::latest('id')->first();
            if ($product){
                $dataDetail = [
                    'order_id'   => $order->id,
                    'product_id' =>$ItemInput['id'],
                    'value'      => $product->value,
                    'amount'     => $ItemInput['cant'],
                    'sub_total'  => ($product->value*$ItemInput['cant']),
                    'created_at'   => Carbon::now(),
                    'updated_at'   => Carbon::now()

                ];

                OrderDetail::create($dataDetail);
            };
        }
        $this->actingAs($user);
        $response = $this->json('POST', $route, $headers);

        $response->assertStatus(200);

        $response->assertJson([
            "res"=> true,
            "cod"=> 200,
            "mes"=> "Exito",
            "det"=> [
                "Operación realizada de forma satisfactoria"
            ],
            "data"=> [
                "Iniciado el Proceso de Compra"
            ]
        ]);
    }
}
