<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{

    public function setUp() :void
    {
        parent::setUp();
        user::truncate();
        User::create([

            'id'        => 1,
            'name'      => "Admin",
            'email'     => "admin@admin.com",
            'password'  => bcrypt('123456'),
            'role_id'   => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now()
        ]);

    }

    public function test_login_ok()
    {

        $route = '/api/v1/user/login';

        $response = $this->post($route, [
            'email' => 'admin@admin.com',
            'password' => '123456',
        ]);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'data' => [
                'user' => [
                    'email',
                    'name',
                ],
                'token' => [
                    'id',
                    'type',
                ],
            ],
        ]);
    }

    public function test_login_not_ok()
    {

        $route = '/api/v1/user/login';

        $response = $this->post($route, [
            'email' => 'xadmin@admin.com',
            'password' => 'clave_invalida',
        ]);

        $response->assertStatus(200)
        ->assertSee('Error');

    }

    public function test_login_fields_empty()
    {
        $route = '/api/v1/user/login';

        $response = $this->json('POST', $route, [
            'email' => '',
            'password' => ''
        ]);

        $response->assertStatus(200)
        ->assertSee(['email' => 'required', 'password' => 'required']);
    }
}
