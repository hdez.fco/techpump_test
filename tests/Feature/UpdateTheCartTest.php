<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Tests\TestCase;
use App\Models\User;
use App\Models\Product;
use Illuminate\Support\Facades\Cache;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateTheCartTest extends TestCase
{
    public function test_can_update_product_to_cart()
    {
        $route = '/api/v1/shopping/update';

        $user = User::find(1);
        $role  = ($user->role_id==1)?'Administrador':'Estandar';

        $token = $user->createToken('TEST',[$role])->plainTextToken;

        $headers = ['Authorization' => "Bearer $token"];

        $this->actingAs($user);

        $product = Product::find(1);

        $payload = [
            [
                'id' => $product->id,
                'cant' => 2,
            ],
        ];

        $response = $this->json('POST', $route,  $payload)
                    ->withHeaders($headers)
                    ;



        $product->refresh();
        $this->assertEquals(8, $product->available);
        $this->assertEquals(2, $product->in_process);

        $payload = [
            [
                'id' => $product->id,
                'cant' => 3,
            ],
        ];

        $response = $this->json('POST', $route,  $payload)
                    ->withHeaders($headers)
                    ;

        Cache::forget('SaleInProcessUser_' . $user->id);
        Cache::rememberForever('SaleInProcessUser_' . $user->id, function() use ($payload){
            return $payload;
        });

        $product->refresh();
        $this->assertEquals(7, $product->available);
        $this->assertEquals(3, $product->in_process);


        $this->assertTrue(Cache::has('SaleInProcessUser_' . $user->id));
        $this->assertEquals($payload, Cache::get('SaleInProcessUser_' . $user->id));
    }
    protected function setUp(): void
    {
        parent::setUp();
        user::truncate();
        User::create([

            'id'        => 1,
            'name'      => "Admin",
            'email'     => "admin@admin.com",
            'password'  => bcrypt('123456'),
            'role_id'   => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now()
        ]);
        Product::truncate();
        Product::create([
            'id' => 1,
            'name' => 'Bicicleta',
            'value' => 1000,
            'image' => null,
            'available' => 10,
            'in_process' =>0,
            'category_id'=> 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now()
            ]);

    }
}
