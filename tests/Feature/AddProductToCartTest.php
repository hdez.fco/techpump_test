<?php

use Carbon\Carbon;
use Tests\TestCase;
use App\Models\User;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Foundation\Testing\WithFaker;
use App\Http\Requests\AddProductTheCarRequest;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AddProductToCartTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;


    public function test_can_add_product_to_cart()
    {
        $route = '/api/v1/shopping/add';

        $user = User::find(1);

        $product = Product::find(1);

        $payload = [
            [
                'id' => $product->id,
                'cant' => 2,
            ],
        ];

        $response = $this->actingAs($user)
                         ->postJson($route, $payload);

        $response->assertStatus(200);

        $product->refresh();
        $this->assertEquals(8, $product->available);
        $this->assertEquals(2, $product->in_process);

        $this->assertTrue(Cache::has('SaleInProcessUser_' . $user->id));
        $this->assertEquals($payload, Cache::get('SaleInProcessUser_' . $user->id));
    }

    protected function setUp(): void
    {
        parent::setUp();
        user::truncate();
        User::create([

            'id'        => 1,
            'name'      => "Admin",
            'email'     => "admin@admin.com",
            'password'  => bcrypt('123456'),
            'role_id'   => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now()
        ]);
        Product::truncate();
        Product::create([
            'id' => 1,
            'name' => 'Bicicleta',
            'value' => 1000,
            'image' => null,
            'available' => 10,
            'in_process' =>0,
            'category_id'=> 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now()
            ]);


        // Define custom validation rule for the AddProductTheCarRequest
        $this->app['validator']->extendImplicit('product_available', function ($attribute, $value, $parameters, $validator) {
            $product = Product::find($value['id']);
            if (!$product || $value['id']==null) {
                return false;
            }

            $available = $product->available - $value['cant'];
            return $available ;
        });
    }
}
