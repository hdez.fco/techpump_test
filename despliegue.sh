#!/bin/bash

php artisan cache:clear
php artisan view:clear
php artisan route:clear
php artisan config:clear

php artisan migrate:refresh
php artisan db:seed

