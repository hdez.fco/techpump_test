<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->truncate();
        DB::table('products')->insert([
            [
            'id' => 1,
            'name' => 'Bicicleta',
            'value' => 1000,
            'image' => null,
            'available' => 20,
            'in_process' =>0,
            'category_id'=> 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now()
            ],
            [
            'id' => 2,
            'name' => 'Caucho',
            'value' => 20,
            'image' => null,
            'available' => 20,
            'in_process' =>0,
            'category_id'=> 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now()
            ],
            [
            'id' => 3,
            'name' => 'Lentes',
            'value' => 10,
            'image' => null,
            'available' => 20,
            'in_process' =>0,
            'category_id'=> 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now()
            ]

            ]);
    }
}
