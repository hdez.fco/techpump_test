<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

            DB::table('users')->truncate();
            DB::table('users')->insert([
                  [
                    'id'        => 1,
                    'name'      => "Admin",
                    'email'     => "admin@admin.com",
                    'password'  => bcrypt('123456'),
                    'role_id'   => 1,
                    'created_at'   => Carbon::now(),
                    'updated_at'   => Carbon::now()
                  ],

            ]);

    }
}
