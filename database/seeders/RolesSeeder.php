<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('roles')->truncate();
        DB::table('roles')->insert([
                ['id' => 1,'name' => "Administrador",'created_at'   => Carbon::now(),'updated_at'   => Carbon::now()],
                ['id' => 2,'name' => "Usuario Estandar",'created_at'   => Carbon::now(),'updated_at'   => Carbon::now()],
            ]);

    }
}
