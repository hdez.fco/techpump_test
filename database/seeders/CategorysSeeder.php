<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CategorysSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

            DB::table('categorys')->truncate();
            DB::table('categorys')->insert([
                  [
                    'id' => 1,'name' => "Ciclismo",'created_at'   => Carbon::now(),'updated_at'   => Carbon::now()
                  ],
                  [
                    'id' => 2,'name' => "Fitness",'created_at'   => Carbon::now(),'updated_at'   => Carbon::now()
                  ],
                ]);

    }
}
